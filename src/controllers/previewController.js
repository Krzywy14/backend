'use strict';

import DB from '../models/baseModel.js';

class PreviewController{
	constructor(){
	}
	async getData( req, res ){
		try{
			if (req.params.fileId === undefined) {
				throw {
					status: 400,
					message: 'File ID not provided'
				};
			}
			
			const resuluts = await DB.getPreviewById(req.params.fileId);
			if (Object.keys(resuluts).length === 0) {
				throw {
					status: 404,
					message: 'File ID not found'
				};
			} else {
				res
					.status(200)
					.send({
						data: resuluts
					});
			}
		} catch (err) {
			res
				.status(err.status)
				.send(err.message);
		}
	}
}

export default new PreviewController;
