/*global global*/

'use strict';

import AWS from 'aws-sdk';
import path from 'path';
import fs from 'fs';
import DB from '../models/baseModel.js';
import worker from '../services/worker.js';

const uploadToS3 = async (file) => {
	return new Promise((resolve, reject) => {
		if( file.name === '' ) return reject({error: 'file not provided'});

		const fileNameOnS3 = `${Date.now()}_${Math.random().toString(36).substring(2, 15)}${path.extname(file.name)}`;
		const s3 = new AWS.S3({
			accessKeyId: 'AKIAJA4FWMGQXTPVVTMA',
			secretAccessKey: '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'
		});
		const s3Params = {
			Bucket: 'krzywy',
			Key: fileNameOnS3,
			Expires: 3600,
			ContentType: file.mimetype,
			ACL: 'public-read',
			Body: fs.createReadStream(file.tempFilePath)
		};
		s3.upload(s3Params, async (err, data) => {
			try{
				if( err ){
					throw new Error({
						status: 500,
						message: err
					});
				}
				const setPreview = await DB.setPreview(file.name, data.Location );
				if( setPreview.previewId ){
					worker.add('createPreview', {
						previewId: setPreview.previewId,
						fileTempPath: file.tempFilePath
					},{
						delay: 5000,
					});
					return resolve({
						...data,
						...setPreview
					});
				}else{
					throw new Error({
						status: 500,
						message: setPreview
					});
				}
			} catch (err) {
				return reject(err);
			}
		});
	});
};

class UploadController{
	constructor(){
		AWS.config.region = 'eu-central-1';
	}
	upload( req, res ){
		try {
			if(!req.files) {
				throw {
					status: 400,
					message: 'No file uploaded'
				};
			} else {
				let socketRoom = global.socketRoom;
				if ( socketRoom === undefined) {
					socketRoom = `${Date.now()}_${Math.random().toString(36).substring(2, 15)}`;
				}
				
				const file = req.files.file;
				uploadToS3(file)
					.then( uploadResult => {
						res
							.status(200)
							.send({
								message: 'File is uploaded',
								data: {
									uploadResult,
									socketRoom: socketRoom
								}
							});
						global.io.to(socketRoom).emit('uploadProgress','Upload the file: completed');
					})
					.catch( err =>{
						global.io.to(socketRoom).emit('uploadProgress','Upload the file: error');
						throw err;
					});
			}
		} catch (err) {
			res
				.status(err.status)
				.send(err.message);
		}
	}
}

export default new UploadController;
