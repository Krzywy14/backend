/*global global, process*/

'use strict';

import express from 'express';
import fileUpload from 'express-fileupload';
import http from 'http';
import socketIo from 'socket.io';
import path from 'path';
import UploadController from './controllers/uploadController.js';
import PreviewController from './controllers/previewController.js';

const app = express();
const port = process.env.PORT || 3000;


const server = http.createServer(app);
const io = socketIo(server);
global.io = io;

app.use(fileUpload({
	useTempFiles : true,
	tempFileDir : 'cache'
}));

global.socketRoom = undefined;

app.get('/', async (req, res) => {
	global.socketRoom = `${Date.now()}_${Math.random().toString(36).substring(2, 15)}`;
	res.sendFile('/public/index.html',{ root: path.resolve(path.dirname('')) });
});


app.post('/upload', (req, res ) => {
	return UploadController.upload(req, res);
});

app.get('/preview/:fileId', (req, res, next) => PreviewController.getData(req, res, next) );


io.on('connection', (client) => {
	client.join(global.socketRoom);
	global.io.to(global.socketRoom).emit('uploadProgress', `Client connet to room: ${global.socketRoom}`);
});

server.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`);
});
