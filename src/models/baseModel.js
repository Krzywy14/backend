import config from '../config.js';
import pg from 'pg';

const { Client } = pg;

class baseModel{
	constructor(){
		this.clientConf = {
			connectionString: config.DB,
			ssl: {
				rejectUnauthorized: false
			}
		};
	}
	async setPreview(fileName, filePath ) {
		try{
			if( fileName === '' ) throw  {error: '{fileName} not provided'};
			if( filePath === '' ) throw new Error('{filePath} not provided');
			
			const client = new Client(this.clientConf);
			
			await client.connect();
			
			const res = await client.query(`INSERT INTO public.files (name, path) VALUES ('${fileName}', '${filePath}' ) RETURNING "ID";`);
	
			await client.end();
			if ( res.rows && res.rows[0] && res.rows[0].ID ) {
				return { previewId: res.rows[0].ID };
			}
			return res;
		} catch (err) {
			return new Error(err);
		}

	}
	async updatePreview(id, filePreview) {
		try{
			if( id === '' ) throw '{id} not provided';
			if( filePreview === '' ) throw '{filePreview} not provided';
			
			const client = new Client(this.clientConf);
			
			await client.connect();
			
			await client.query(`UPDATE public.files SET preview = '${filePreview}' WHERE "ID" = ${id} `);
	
			await client.end();

			return true;
		} catch (err) {
			return new Error(err);
		}

	}
	async getPreviewById(id) {
		try{
			if( !id ) throw '{id} not provided';

			const client = new Client(this.clientConf);

			await client.connect();

			const res = await client.query(`SELECT * FROM public.files WHERE "ID" = ${id};`);

			await client.end();

			if ( res.rows ) {
				return res.rows;
			}
			return res;

		} catch (err) {
			return err;
		}
	}
}

export default new baseModel;