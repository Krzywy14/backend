/*global global*/
import config from '../config.js';

import Bull from 'bull';
import fs from 'fs';
import readline from 'readline';
import DB from '../models/baseModel.js';

const FileProcess = new Bull('FileProcess', {redis: config.REDIS});

FileProcess.process('createPreview', async (job) => { 
	try {
		const DATA = job.data;
		const lineReader = readline.createInterface({
			input: fs.createReadStream(DATA.fileTempPath),
		});
		let lineCounter = 0;
		let wantedLines = [];
		await lineReader.on('line', (line) => {
			lineCounter++;
			wantedLines.push(line);
			if(lineCounter === 6){
				lineReader.close();
				lineReader.removeAllListeners();
			}
		});
		return Promise.resolve({
			previewId: DATA.previewId,
			wantedLines: wantedLines,
			fileTempPath: DATA.fileTempPath,
		});
	} catch(err) {
		return Promise.reject(err);
	}
});

FileProcess.on('error', () => {
	global.io.to(global.socketRoom).emit('uploadProgress','Processing the file: error');
});

FileProcess.on('waiting', async () => {
	global.io.to(global.socketRoom).emit('uploadProgress','Processing the file: waiting');
});
FileProcess.on('active', async () => {
	global.io.to(global.socketRoom).emit('uploadProgress','Processing the file: started');
});

FileProcess.on('completed', async (job, result) => {
	try{
		const res = await DB.updatePreview(result.previewId, result.wantedLines);
		if (res === true &&  result.fileTempPath) {
			fs.unlink( result.fileTempPath, (err) => {
				if (err) {
					throw new Error(err);
				}
			});
		}
		console.log('completed', res);
		global.io.to(global.socketRoom).emit('uploadProgress','Processing the file: completed');
	} catch(err) {
		console.log('error', err);
	}
});

FileProcess.on('failed', (job, err) => {
	global.io.to(global.socketRoom).emit('uploadProgress','Processing the file: faild');
	console.log(job,err);
});
export default FileProcess;
